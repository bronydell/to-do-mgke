package ru.equestriadev.mgke;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.yandex.metrica.YandexMetrica;

public class PupilFragment extends Fragment {
	private boolean faving;
	private int position;
	private boolean show, offline;
	View rootView;
	ActionBar bar;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		YandexMetrica.reportEvent("Запущен ученик");
		rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		
		return rootView;
	}
	
	public static PupilFragment newInstance(int someInt) {
		PupilFragment myFragment = new PupilFragment();

	    Bundle args = new Bundle();
	    args.putInt("someInt", someInt);
	    myFragment.setArguments(args);

	    return myFragment;
	}
	
	 @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    // Initialize the Fragment.	    
	    	
	    if (savedInstanceState != null) {
			position = savedInstanceState.getInt("GetOpened");
			faving= savedInstanceState.getBoolean("Faving");
			if(position>0)
			offline=true;
	    }
	    else {
			position = 0;
			offline=false;
		}
	  }
	 
	 @Override
	 public void onStart()
	 {
		 super.onStart();
		 if(bar!=null)
		 {
		 bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		 bar.setTitle("Расписание для учеников");
		 }
		 View view = getView();
		 prs(isFaving(), false, true);
		    if(view==null)
		    {
				Log.d("Debug", "View null O_o?");
		    }
		 final SwipeRefreshLayout layout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
		 layout.setOnRefreshListener(new OnRefreshListener() {

			 @Override
			 public void onRefresh() {
				 // TODO Auto-generated method stub
				 layout.setRefreshing(true);
				 prs(isFaving(), offline, true);
			 }
		 });
	 }
	@Override
	public void onAttach(Activity activity) {
		  super.onAttach(activity);
		  ((MainActivity) activity).onSectionAttached(
		     0);
		bar=((ActionBarActivity)activity).getSupportActionBar();
		
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d("Debug", "MainActivity: onResume()");
	}

	public void prs(boolean fav, boolean off, boolean show)
	{
	    ParsePupil pupil = new ParsePupil();
	    
	    pupil.setContext(getActivity().getApplicationContext());
	    
	    if(getView()!=null)
		pupil.setUI((SwipeRefreshLayout) getView().findViewById(R.id.refresh), (ExpandableListView) getView().findViewById(R.id.expandableListView1),
				(TextView) getView().findViewById(R.id.textView1));
		pupil.setFaving(fav);
		pupil.setActivity(getActivity());
		pupil.setOffline(off);
		pupil.setposition(position);
	    pupil.setShow(show);
	    pupil.execute();
	    
	}
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		Log.d("Debug", "OnSaveState");
		ExpandableListView listView =  (ExpandableListView)getView().findViewById(R.id.expandableListView1);
		savedInstanceState.putInt("GetOpened", listView.getLastVisiblePosition());
		savedInstanceState.putBoolean("Faving", faving);
		
	}

	/**
	 * @return the faving
	 */
	public boolean isFaving() {
		return faving;
	}

	/**
	 * @param faving the faving to set
	 */
	public void setFaving(boolean faving) {
		this.faving = faving;
	}
	
	public void setShow(boolean show) {
		this.show = show;
	}
}
