package ru.equestriadev.mgke;

import java.util.ArrayList;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ExpListAdapter extends BaseExpandableListAdapter {

    private ArrayList<Map<String, String>> mGroupData;
    private Context mContext;
    private ArrayList<ArrayList<Map<String, String>>> mChildData;
    String groupFrom[];
    String mChildFrom[];
    public ExpListAdapter (Context context, ArrayList<Map<String, String>> groups, String groupFrom[], ArrayList<ArrayList<Map<String, String>>> childData, String childFrom[]){
        mContext = context;
        this.groupFrom=groupFrom;
        this.mChildFrom=childFrom;
        mGroupData = groups;
        this.mChildData=childData;
    }
    
    public int getChildrenCount(int groupPosition) {
        return mChildData.get(groupPosition).size();
    }

    public Object getGroup(int groupPosition) {
        return mGroupData.get(groupPosition);
    }

    public int getGroupCount() {
        return mGroupData.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
    
    public Object getChild(int groupPosition, int childPosition) {
        return mChildData.get(groupPosition).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group_view, null);
        }


        TextView textGroup = (TextView) convertView.findViewById(R.id.textGroup);
        textGroup.setText(mGroupData.get(groupPosition).get(groupFrom[0]));

        final ImageButton button = (ImageButton)convertView.findViewById(R.id.imageButton1);
        SharedPreferences myPrefs = mContext.getSharedPreferences("Settings", Context.MODE_PRIVATE);
    	
        if(myPrefs.getBoolean(mGroupData.get(groupPosition).get(groupFrom[0]), false))
		{
			button.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fave_44x44on));
		}
		else
		{
			button.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fave_44x44));
		}
        button.setFocusable(false);  
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	//Toast.makeText(mContext, "WoW", Toast.LENGTH_SHORT).show();
            	SharedPreferences myPrefs = mContext.getSharedPreferences("Settings", Context.MODE_PRIVATE);
            	Editor edit = myPrefs.edit();
        		if(!myPrefs.getBoolean(mGroupData.get(groupPosition).get(groupFrom[0]), false))
        		{
        			edit.putBoolean(mGroupData.get(groupPosition).get(groupFrom[0]), true);
        			button.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fave_44x44on));
        		}
        		else
        		{
        			edit.putBoolean(mGroupData.get(groupPosition).get(groupFrom[0]), false);
        			button.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fave_44x44));
        		}
        			edit.commit();
            }
        });
        
        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.child_view, null);
        }
        Log.d("Debug", mChildData.get(groupPosition).get(childPosition).get(mChildFrom[0]));
        TextView textChild = (TextView) convertView.findViewById(R.id.textChild);
        textChild.setText(mChildData.get(groupPosition).get(childPosition).get(mChildFrom[0]));
        convertView.startAnimation(AnimationUtils.loadAnimation(mContext,R.anim.abc_fade_in));
        
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}