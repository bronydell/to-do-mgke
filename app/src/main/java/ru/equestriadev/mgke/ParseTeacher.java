package ru.equestriadev.mgke;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParseTeacher extends AsyncTask<Boolean, Void, Void> {
    String date;
    ArrayList<String> teachers;
    HashMap<String, ArrayList<String>> lessons;

    boolean offliner, faving, showing;
    Context cont;
    private int pos = 0;
    private SwipeRefreshLayout layout;
    private SharedPreferences myPrefs;
    private ExpandableListView listView;
    private TextView dates;
    private String URL = "http://mgke.minsk.edu.by/ru/main.aspx?guid=3821";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // bar.setVisibility(View.VISIBLE);
        teachers = new ArrayList<String>();
        lessons = new HashMap<String, ArrayList<String>>();
        myPrefs = cont.getSharedPreferences("Settings", Context.MODE_PRIVATE);
    }

    public static boolean checkNetworkStatus(Context context) {
        ConnectivityManager cn = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected() == true;

    }

    @Override
    protected Void doInBackground(Boolean... mod) {
        Document document = null;
        if (layout != null) {
            Handler handler = new Handler(cont.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    int colors[] =
                            {
                                    R.color.first,
                                    R.color.second,
                                    R.color.Threes,
                                    R.color.Fours,
                            };
                    layout.setColorScheme(colors);
                    if (showing)
                        layout.setRefreshing(true);
                }
            });
        }
        if (!checkNetworkStatus(cont) || offliner) {
            offline();
        } else {

            try {
                document = Jsoup.connect(URL).timeout(0).get();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            online(document);
        }

        return null;

    }

    @Override
    protected void onPostExecute(Void result) {
        // Set title into TextView

        date = myPrefs.getString("DateT", "Error");
        if (dates != null)
            dates.setText(date);
        ArrayList<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        for (String group : teachers) {
            // заполняем список атрибутов для каждой группы
            if (!faving || myPrefs.getBoolean(group, false)) {
                HashMap<String, String> m = new HashMap<String, String>();
                m.put("groupName", group);
                groupData.add(m);
            }
        }
        // список атрибутов групп для чтения
        String groupFrom[] = new String[]{"groupName"};
        ArrayList<ArrayList<Map<String, String>>> childData = new ArrayList<ArrayList<Map<String, String>>>();

        for (int i = 0; i < teachers.size(); i++) {
            if (!faving || myPrefs.getBoolean(teachers.get(i), false)) {
                ArrayList<Map<String, String>> childDataItem = new ArrayList<Map<String, String>>();
                String[] array = lessons.get(teachers.get(i)).toArray(
                        new String[lessons.get(teachers.get(i)).size()]);
                for (String lesson : array) {
                    HashMap<String, String> m = new HashMap<String, String>();
                    m.put("Above", lesson);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
            }
        }
        // список аттрибутов элементов для чтения
        String childFrom[] = new String[]{"Above"};
        ExpListAdapter adapter = new ExpListAdapter(cont, groupData, groupFrom,
                childData, childFrom);

        if (listView != null) {
            listView.setAdapter(adapter);
            listView.smoothScrollToPosition(pos);
        }

        if (layout != null)
            layout.setRefreshing(false);
    }

    public void setFaving(boolean faving) {
        this.faving = faving;
    }

    public void setOffline(boolean offline) {
        this.offliner = offline;
    }

    public void setContext(Context cont) {
        this.cont = cont;
    }

    public void setUI(SwipeRefreshLayout swipeRefreshLayout, ExpandableListView list, TextView dateview) {
        this.layout = swipeRefreshLayout;
        this.listView = list;
        this.dates = dateview;
    }

    public void setposition(int position) {
        pos = position;
    }

    public void setShow(boolean showing) {
        this.showing = showing;
    }

    public HashMap<String, ArrayList<String>> getlessons() {
        return lessons;
    }

    public void offline() {
        String json;
        json = myPrefs.getString("LastSaveParseT", "");

        if (json.length() <= 0) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            cont.startActivity(intent);
            this.cancel(true);
            if (myPrefs.getBoolean("Kaz", true)) {
                myPrefs.edit().putBoolean("Kaz", false).commit();
            }
            myPrefs.edit().putBoolean("Kaz", true).commit();
            Handler handler = new Handler(cont.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(cont,
                            "Включите интернет. Нет сохраненной копии",
                            Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            ru.equestriadev.mgke.Map mapper = new ru.equestriadev.mgke.Map();
            mapper = new Gson().fromJson(json,
                    ru.equestriadev.mgke.Map.class);
            if (mapper.Map != null && mapper.group != null) {
                lessons = mapper.Map;
                teachers = mapper.group;
            } else {
                this.cancel(true);
            }
        }
    }

    public void online(Document document) {
        Element content = null;
        if (document != null)
            content = document.getElementsByClass("content")
                    .first();


        if (content == null) {
            Handler handler = new Handler(cont.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(cont, "Ошибка. Контент не найден. Попробуйте потянуть обновиться(свайп сверху вниз)", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Element tbody = content.select("tbody").first();
            Elements elements = tbody.select("tr");
            try {
                date = content.select("thead").first().select("tr").first().select("td").get(2).text();
            } catch (Exception e) {
                date = "???";
            }

            myPrefs.edit().putString("DateT", date).commit();
            for (int k = 0; k < elements.size(); k++) {
                Elements tds = elements.get(k).select("td"); //Constant
                ArrayList<String> pairs = new ArrayList<String>();
                int num = 1;
                for (int i = 2; i < tds.size(); i = i + 2)
                    if (tds.get(1).text().replace("\u00A0", "")
                            .length() > 0) {


                        if (tds.get(i).text().length() > 1
                                && i + 1 < tds.size()) {
                            pairs.add(num + ". Группа "
                                    + tds.get(i).text()
                                    + ". В кабинете "
                                    + tds.get(i + 1).text());
                        } else
                            pairs.add(num + ". Нет пары");

                        num++;


                    }
                lessons.put(tds.get(1).text(),
                        pairs);
                teachers.add(tds.get(1).text());
            }
        }
        Editor prefsEditor = myPrefs.edit();
        Gson gson = new Gson();
        ru.equestriadev.mgke.Map mapper = new ru.equestriadev.mgke.Map();
        mapper.Map = lessons;
        mapper.group = teachers;
        String json = gson.toJson(mapper);
        prefsEditor.putString("LastSaveParseT", json);
        prefsEditor.commit();
    }

}
