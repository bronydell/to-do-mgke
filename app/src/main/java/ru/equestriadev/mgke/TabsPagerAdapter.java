package ru.equestriadev.mgke;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class TabsPagerAdapter extends FragmentStatePagerAdapter {
	int pos;
	private ArrayList<Fragment> fragments=new ArrayList<Fragment>();
	private FragmentManager fm;
	int count;
	public TabsPagerAdapter(FragmentManager fm, int position) {
		super(fm);
		Log.d("Debug", "super");
		pos=position;
		if(position==2)
			count=1;
		else
			count=2;
		Log.d("Debug", "Position: " + pos);
	    this.fm=fm;
    }
	
	public void setPosition(int position)
	{
		pos=position;
	}
    @Override
    public Fragment getItem(int index) {
    	Log.d("Debug", "getItem dogy. With pos = "+ pos +" and index = "+ index);
    	   	switch(pos)
    	    	{
    	    	case 0:
    	    		PupilFragment frag= new PupilFragment().newInstance(0);
    	    		fragments.add(frag);
    	    		 switch (index) {
    	    	        case 0:
    	    	        	// Top Rated fragment activity
    	    	        	frag.setFaving(false);
    	    	            return frag;
    	    	        case 1:
    	    	            // Games fragment activity
    	    	        	frag.setFaving(true);
    	    	        	return frag;
    	    	        default:
    	    	        	Log.d("Debug", "WTF?");
    	    	        	break;
    	    	        }
    	    		
    	    	case 1:
    	    		Personal pers= new Personal().newInstance(1);
    	    		fragments.add(pers);
   	    		 switch (index) {
   	    	        case 0:
   	    	        	pers.setFaving(false);
   	    	            return pers;
   	    	        case 1:
   	    	            pers.setFaving(true);
   	    	        	return pers;
   	    	        }
   	    		break;
    	    	case 2:
    	    		Settings settings = new Settings().newInstance(2);
    	    		fragments.add(settings);
    	    		return settings;
    	    	}
       return null;
    }
    
    public void clearAll() //Clear all page
    {
      for(int i=0; i<fragments.size(); i++)
      fm.beginTransaction().remove(fragments.get(i)).commit();
      fragments.clear();
    }
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return count;
    }

}
