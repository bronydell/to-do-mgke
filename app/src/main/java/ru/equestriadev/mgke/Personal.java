package ru.equestriadev.mgke;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.yandex.metrica.YandexMetrica;

public class Personal extends Fragment {

    boolean faving;
    private int position;
    private ActionBar bar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal,
                container, false);
        YandexMetrica.reportEvent("Запущен ученик");
        return view;
    }

    public static Personal newInstance(int someInt) {
        Personal myFragment = new Personal();

        Bundle args = new Bundle();
        args.putInt("someInt", someInt);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            position = savedInstanceState.getInt("GetOpened");
            faving = savedInstanceState.getBoolean("Faving");
        } else
            position = 0;
        Log.d("Debug", "onActivityCreated " + position);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (bar != null) {
            bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            bar.setTitle("Расписание для учителей");
        }
        View view = getView();
        Log.d("Debug", "OnStart");
        prs(faving, false, false);
        if (view == null) {
            Log.d("Debug", "View null O_o?");
        }
        final SwipeRefreshLayout layout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        layout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub
                layout.setRefreshing(true);

                prs(faving, false, false);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(1);
        bar = ((ActionBarActivity) activity).getSupportActionBar();
    }

    public void prs(boolean fav, boolean off, boolean show) {
        ParseTeacher pupil = new ParseTeacher();
        Log.d("Debug", "Start prs with: " + fav + " faving and " + off + "offline and show " + show);
        pupil.setContext(getActivity().getApplicationContext());
        pupil.setUI((SwipeRefreshLayout) getView().findViewById(R.id.refresh), (ExpandableListView) getView().findViewById(R.id.expandableListView1),
                (TextView) getView().findViewById(R.id.textView1));
        pupil.setFaving(fav);
        pupil.setposition(position);
        pupil.setOffline(off);
        pupil.setShow(show);
        pupil.execute();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        ExpandableListView listView = (ExpandableListView) getView().findViewById(R.id.expandableListView1);
        savedInstanceState.putInt("GetOpened", listView.getFirstVisiblePosition());
        savedInstanceState.putBoolean("Faving", faving);
    }

    /**
     * @return the faving
     */
    public boolean isFaving() {
        return faving;
    }

    /**
     * @param faving the faving to set
     */
    public void setFaving(boolean faving) {
        this.faving = faving;
    }
}
