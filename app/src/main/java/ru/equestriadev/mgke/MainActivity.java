package ru.equestriadev.mgke;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.yandex.metrica.YandexMetrica;

public class MainActivity extends ActionBarActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        ActionBar.TabListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the
     * navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ActionBar actionbar;
    ViewPager viewPager;
    /**
     * Used to store the last screen title. For use in
     * {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private int po;
    FragmentManager fragmentManager;
    TabsPagerAdapter mAdapter;
    Bundle savedInstanceStategl;
    SharedPreferences myPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        YandexMetrica.activate(getApplicationContext(), "10b73f7b-e0f2-49dd-b27f-545687ea021e");
        //optional - allows more than on notifications in the status bar, default is false
        Log.d("Debug", "MainActivty OnCreate");
        myPrefs = getApplicationContext().getSharedPreferences("Settings", MODE_PRIVATE);
        if (!myPrefs.contains("Kaz")) {
            showDialog();
            myPrefs.edit().putBoolean("Dialog", true).commit();
        }
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigation_drawer);
        fragmentManager = getSupportFragmentManager();
        mTitle = getTitle();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        actionbar = getSupportActionBar();
        if (savedInstanceState == null && myPrefs.getBoolean("Teach", false))
            onNavigationDrawerItemSelected(1, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        YandexMetrica.onResumeActivity(this);
    }

    @Override
    protected void onPause() {
        YandexMetrica.onPauseActivity(this);
        super.onPause();
    }

    public void showDialog() {
        final String[] mCatsName = {"Казинца", "Кнорина", "Я учитель"};

        Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выбераем корпус"); // заголовок для диалога

        builder.setItems(mCatsName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                // TODO Auto-generated method stub
                Log.d("Debug", "Setting item is " + item);
                switch (item) {
                    case 0:
                        myPrefs.edit().putBoolean("Kaz", true).commit();
                        break;
                    case 1:
                        myPrefs.edit().putBoolean("Kaz", false).commit();
                        break;
                    case 2:
                        myPrefs.edit().putBoolean("Teach", true).commit();
                    default:
                        break;
                }


            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Debug", "OnStart");


        viewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new TabsPagerAdapter(fragmentManager, po);
        viewPager.setAdapter(mAdapter);
        viewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        getSupportActionBar().setSelectedNavigationItem(position);
                    }
                });
        setupTabs();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position, boolean fromSavedInstanceState) {
        // update the main content by replacing fragments
        po = position;
        Log.d("Debug", "onNavigationDrawerItemSelected called");
        if (mAdapter != null) {
            Log.d("Debug", "mAdapter!=null. Position " + po);
            viewPager.removeAllViews();
            viewPager.setAdapter(null);
            mAdapter = new TabsPagerAdapter(fragmentManager, po);
            viewPager.setAdapter(mAdapter);
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowTitleEnabled(true);
        //actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.

            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void setupTabs() {

        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        if (actionbar.getTabCount() < 2) {
            Tab tab1 = actionbar.newTab().setText("Всё расписание")
                    .setTabListener(this);

            actionbar.addTab(tab1);

            Tab tab2 = actionbar.newTab().setText("Избранное")
                    .setTabListener(this);
            actionbar.addTab(tab2);
        }
    }

    @Override
    public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction arg1) {
        // TODO Auto-generated method stub
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }
}

	