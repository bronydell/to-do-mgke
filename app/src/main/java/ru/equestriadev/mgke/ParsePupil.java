package ru.equestriadev.mgke;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ParsePupil extends AsyncTask<Boolean, Void, Void> {
    String date;
    ArrayList<String> groups;
    HashMap<String, ArrayList<String>> lessons;
    Document document;
    boolean offliner, faving, showing;
    Context cont;
    private int pos = 0;
    private SwipeRefreshLayout layout;
    private Activity activity;
    private ExpListAdapter adapter;
    private SharedPreferences myPrefs;
    private ExpandableListView listView;
    private TextView dates;
    private String URLkn = "http://mgke.minsk.edu.by/ru/main.aspx?guid=3841";
    private String URLkaz = "http://mgke.minsk.edu.by/ru/main.aspx?guid=3831";

    private String URLCurr;

    private String feedingkaz[] = {"08:30 - 10:10", "10:20 - 12:10",
            "12:30 - 14:10", "14:20 - 16:00", "16:20 - 17:40", "17:50 - 19:10",
            "19:20 - 20:40"};
    private String feedingkazsa[] = {"08:30 - 9:50", "10:00 - 11:45",
            "12:00 - 13:20", "13:30 - 14:50", "15:20 – 16:40", "16.50 – 18.10"};
    private String feedingkn[] = {"09:00 - 10:40", "11:00 - 12:50",
            "13:10 - 14:50", "15:00 - 16:40"};

    public static boolean checkNetworkStatus(Context context) {
        ConnectivityManager cn = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected() == true;

    }

    public void setposition(int position) {
        pos = position;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // bar.setVisibility(View.VISIBLE);
        groups = new ArrayList<String>();
        lessons = new HashMap<String, ArrayList<String>>();
        myPrefs = cont.getSharedPreferences("Settings", Context.MODE_PRIVATE);
    }

    @Override
    protected Void doInBackground(Boolean... mod) {
        document = null;
        if (layout != null) {
            Handler handler = new Handler(cont.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    int colors[] = {R.color.first, R.color.second,
                            R.color.Threes, R.color.Fours,};
                    layout.setColorScheme(colors);
                    layout.setRefreshing(true);
                }
            });
        }
        if (!checkNetworkStatus(cont) || offliner) {
            offline();

        } else {

            try {
                if (myPrefs.getBoolean("Kaz", true)) {
                    URLCurr=URLkaz;
                    document = Jsoup.connect(URLkaz).timeout(10000).get();
                } else {
                    URLCurr=URLkn;

                    document = Jsoup.connect(URLkn).timeout(10000).get();

                }
                try {
                    online(document);
                } catch (final Exception e) {
                    // TODO: handle exception
                    Handler handler = new Handler(cont.getMainLooper());
                    handler.post(new Runnable() {
                        public void run() {
                            Toast.makeText(cont, "Error: " + e.toString(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Handler handler = new Handler(cont.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(cont, "Error: " + e.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        // Set title into TextView

        date = myPrefs.getString("Date", "Error");
        if (dates != null)
            dates.setText(date);
        ArrayList<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        for (String group : groups) {
            // заполняем список атрибутов для каждой группы
            if (!faving || myPrefs.getBoolean(group, false)) {
                HashMap m = new HashMap<String, String>();
                m.put("groupName", group);
                groupData.add(m);
            }
        }
        // список атрибутов групп для чтения
        String groupFrom[] = new String[]{"groupName"};
        // список ID view-элементов, в которые будет помещены аттрибуты
        // групп
        int groupTo[] = new int[]{android.R.id.text1};
        ArrayList<ArrayList<Map<String, String>>> childData = new ArrayList<ArrayList<Map<String, String>>>();

        for (int i = 0; i < groups.size(); i++) {
            if (!faving || myPrefs.getBoolean(groups.get(i), false)) {
                ArrayList<Map<String, String>> childDataItem = new ArrayList<Map<String, String>>();
                String[] array = lessons.get(groups.get(i)).toArray(
                        new String[lessons.get(groups.get(i)).size()]);
                for (String lesson : array) {
                    HashMap m = new HashMap<String, String>();
                    m.put("Above", lesson);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
            }
        }
        // список аттрибутов элементов для чтения
        String childFrom[] = new String[]{"Above"};
        // список ID view-элементов, в которые будет помещены аттрибуты
        // элементов
        int childTo[] = new int[]{android.R.id.text1};
        adapter = new ExpListAdapter(cont, groupData, groupFrom,
                childData, childFrom);
        if (listView != null) {
            listView.setAdapter(adapter);
            listView.setSelection(pos);
        }
        if (listView != null)
            listView.setOnChildClickListener(new OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
                    // TODO Auto-generated method stub
                    FromTwo to = new FromTwo();
                    Calendar calendar = Calendar.getInstance();
                    if (childPosition < feedingkaz.length)
                        if (myPrefs.getBoolean("Kaz", true)) {

                            if (calendar.get(Calendar.DAY_OF_WEEK) != 7) {
                                SimpleDateFormat s = new SimpleDateFormat(
                                        "HH:mm");
                                String format = s.format(new Date());
                                String times[] = feedingkaz[childPosition]
                                        .split(" - ");
                                try {
                                    Toast.makeText(cont,
                                            to.Sub(format, times[0], times[1]),
                                            Toast.LENGTH_SHORT).show();
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            } else {
                                if (childPosition < feedingkazsa.length) {
                                    SimpleDateFormat s = new SimpleDateFormat(
                                            "HH:mm");
                                    String format = s.format(new Date());
                                    String times[] = feedingkazsa[childPosition]
                                            .split(" - ");
                                    try {
                                        Toast.makeText(
                                                cont,
                                                to.Sub(format, times[0],
                                                        times[1]),
                                                Toast.LENGTH_SHORT).show();
                                    } catch (ParseException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            if (childPosition < feedingkn.length) {
                                SimpleDateFormat s = new SimpleDateFormat(
                                        "HH:mm");
                                String format = s.format(new Date());
                                String times[] = feedingkn[childPosition]
                                        .split(" - ");
                                try {
                                    Toast.makeText(cont,
                                            to.Sub(format, times[0], times[1]),
                                            Toast.LENGTH_SHORT).show();
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }
                    return false;
                }
            });

        if (layout != null)
            layout.setRefreshing(false);
    }

    public void setFaving(boolean faving) {
        this.faving = faving;
    }

    public void setOffline(boolean offline) {
        this.offliner = offline;
    }

    public void setContext(Context cont) {
        this.cont = cont;
    }

    public void setShow(boolean showing) {
        this.showing = showing;
    }

    public void setUI(SwipeRefreshLayout swipeRefreshLayout,
                      ExpandableListView list, TextView dateview) {
        this.layout = swipeRefreshLayout;
        this.listView = list;
        this.dates = dateview;
    }

    public HashMap<String, ArrayList<String>> getlessons() {
        return lessons;
    }

    public void online(Document document) {
        Element content = null;
        if (document != null)
            content = document.getElementsByClass("content").first();
        if (content == null) {
            Handler handler = new Handler(cont.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(
                            cont,
                            "Ошибка. Контент не найден. Попробуйте потянуть обновиться(свайп сверху вниз)",
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Elements tables = content.select("table");
            int all = 0;

            date = tables.get(0).select("tr").get(0).text();
            myPrefs.edit().putString("Date", date).commit();
            for (int k = 1; k < tables.size() + 1; k++) {
                Element e = tables.get(k - 1);
                Elements elements = e.select("tr");
                int groupcount = 0;
                Elements namelab = elements.get(0).select("td"); // Constant
                boolean dated = false;
                if (elements.get(0).select("td").size() <= 1) {
                    namelab = elements.get(1).select("td");
                    dated = true;
                }
                for (int i = 1; i < namelab.size(); i++)
                    if (namelab.get(i).text().replace("\u00A0", "").length() > 0) {
                        // Group adding

                        groupcount++;
                        ArrayList<String> pairs = new ArrayList<String>();
                        int position = groupcount * 2 - 1;
                        int num = 1;
                        int j = 2;
                        if (dated)
                            j = 3;

                        for (; j < elements.size(); j++) {
                            Elements tds = elements.get(j).select("td");
                            if (tds.get(position).text().length() > 1
                                    && position + 1 < tds.size()) {
                                pairs.add(num + ". " + tds.get(position).text()
                                        + ". В кабинете(ах) "
                                        + tds.get(position + 1).text());
                            } else
                                pairs.add(num + ". Нет пары");
                            tds = null;
                            num++;
                        }

                        lessons.put(namelab.get(i).text(), pairs);
                        groups.add(namelab.get(i).text());
                        pairs = null;
                    }

            }
            if (adapter != null)
                adapter.notifyDataSetChanged();
            Save();
        }
    }

    public void offline() {
        String json;
        if (myPrefs.getBoolean("Kaz", true)) {
            json = myPrefs.getString("LastSaveParse", "");

        } else {
            json = myPrefs.getString("LastSaveParsekn", "");
        }

        if (json.length() <= 0) {

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            cont.startActivity(intent);
            this.cancel(true);
            if (myPrefs.getBoolean("Kaz", true)) {
                myPrefs.edit().putBoolean("Kaz", false).commit();
            }
            myPrefs.edit().putBoolean("Kaz", true).commit();
            Handler handler = new Handler(cont.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(cont,
                            "Включите интернет. Нет сохраненной копии",
                            Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            ru.equestriadev.mgke.Map mapper = new ru.equestriadev.mgke.Map();
            mapper = new Gson().fromJson(json, ru.equestriadev.mgke.Map.class);
            if (mapper.Map != null && mapper.group != null) {
                lessons = mapper.Map;
                groups = mapper.group;
            } else {
                this.cancel(true);
            }
        }
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public void Save() {

        Editor prefsEditor = myPrefs.edit();
        Gson gson = new Gson();
        ru.equestriadev.mgke.Map mapper = new ru.equestriadev.mgke.Map();
        mapper.Map = lessons;
        mapper.group = groups;
        String json = gson.toJson(mapper);

        if (myPrefs.getBoolean("Kaz", true)) {

            prefsEditor.putString("LastSaveParse", json);
        } else {
            prefsEditor.putString("LastSaveParsekn", json);
        }

        prefsEditor.commit();
    }

    public String getLastDate() {
        Element cont = document.getElementsByClass("content").first();
        Elements tables = cont.select("table");
        Element e = tables.get(0);
        Elements elements = e.select("tr");
        return elements.get(0).text();
    }

    public void setActivity(Activity act) {
        activity = act;
    }
}
