package ru.equestriadev.mgke;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavAdapter extends ArrayAdapter<String> {
		private final Context context;
	    private final String[] names;
	    private final int[] images;
	    public NavAdapter(Context context, String[] names, int[] images) {
	        super(context, R.layout.nav_view, names);
	        this.context = context;
	        this.names = names;
	        this.images = images;
	    }

	    // Класс для сохранения во внешний класс и для ограничения доступа
	    // из потомков класса
	    static class ViewHolder {
	        public ImageView imageView;
	        public TextView textView;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        // ViewHolder буферизирует оценку различных полей шаблона элемента

	        ViewHolder holder;
	        // Очищает сущетсвующий шаблон, если параметр задан
	        // Работает только если базовый шаблон для всех классов один и тот же
	        View rowView = convertView;
	        if (rowView == null) {
	        	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            rowView = inflater.inflate(R.layout.nav_view, null, true);
	            holder = new ViewHolder();
	            holder.textView = (TextView) rowView.findViewById(R.id.Itemname);
	            holder.imageView = (ImageView) rowView.findViewById(R.id.icon);
	            rowView.setTag(holder);
	        } else {
	            holder = (ViewHolder) rowView.getTag();
	        }

	        holder.textView.setText(names[position]);
	        String s = names[position];
	        holder.imageView.setImageResource(images[position]);
	        return rowView;
	    }
	}