package ru.equestriadev.mgke;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class Settings extends Fragment {
	View rootView;
	CheckBox box2;
	Spinner spin;
	SharedPreferences prefs;
	ActionBar bar;
	String[] data = {"Казинца", "Кнорина"};
	Button butt;
	TextView ver, bild; 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.settings, container,
				false);
		
		return rootView;
	}
	public static Settings newInstance(int someInt) {
		Settings myFragment = new Settings();

	    Bundle args = new Bundle();
	    args.putInt("someInt", someInt);
	    myFragment.setArguments(args);

	    return myFragment;
	}
	 @Override
	  public void onStart() {
	    super.onStart();
	    
	    prefs = getActivity().getSharedPreferences("Settings", getActivity().getApplicationContext().MODE_PRIVATE);
	   PackageInfo pInfo=null;
	    try {
			pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    ver=(TextView)getView().findViewById(R.id.textView1);
	    ver.setText("Версия: "+pInfo.versionName);
	    bild=(TextView)getView().findViewById(R.id.textView2);
	    bild.setText("Номер сборки: "+ pInfo.versionCode);
	    butt = (Button)getView().findViewById(R.id.button1);
	    butt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"littleponyapps@gmail.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, "Расписание МГКЭ");
				try {
				    startActivity(Intent.createChooser(i, "Отправить письмо..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getActivity(), "Нет почтового клиента :(.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	    box2=(CheckBox)getView().findViewById(R.id.checkBox2);
	    spin=(Spinner)getView().findViewById(R.id.spinner1);
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, data);
        spin.setAdapter(adapter);
        spin.setPadding(0, 10, 0, 10);
        spin.setPrompt("Выбрать корпус");
        if(prefs.getBoolean("Kaz", true))
        	spin.setSelection(0);
        else
        	spin.setSelection(1);
        spin.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                int position, long id) {
              // показываем позиция нажатого элемента
              if(position==0)
              {
            	  prefs.edit().putBoolean("Kaz", true).commit();
              }
              else
            	  prefs.edit().putBoolean("Kaz", false).commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
          });
	   box2.setChecked(prefs.getBoolean("Teach", false));
	   box2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(box2.isChecked())
				{
					prefs.edit().putBoolean("Teach", false).commit();
				}
				else
				prefs.edit().putBoolean("Teach", true).commit();
			}
		});
	   }
	   
	 
	 @Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			bar=((ActionBarActivity)activity).getSupportActionBar();
			bar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	    	bar.setTitle("Настройки");
		}
	
}
